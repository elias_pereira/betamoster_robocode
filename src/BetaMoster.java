package betamoster;
import robocode.AdvancedRobot;
import robocode.HitRobotEvent;

import robocode.ScannedRobotEvent;


import java.awt.*;

public class BetaMoster extends AdvancedRobot {
	int turnDirection = 1; //Utilizada para Girar

	public void run() 
	{
		// Inicializacao do robo
		// Cor do Corpo
		// Cor da Arma
		// Cor do Radar
		setBodyColor(Color.red);
	   	setGunColor(Color.blue);
		setRadarColor(Color.black);
		
		// Cor do Scan
		// Cor da Bala
		setScanColor(Color.yellow);
		setBulletColor(Color.red);

		while (true) 
		{ 
		    ahead(50);						//Andar 
			turnRight(5 * turnDirection);   //Girar 
			turnLeft(90);					//Muda de Direção
		    setMaxVelocity(5);
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) 
	{
		if (e.getBearing() >= 0) { //caso o ângulo seja maior do que 0
		turnDirection = 1; //ele gira em sentido horário
		} 
		else 
		{
		turnDirection = -1; //caso contrário, sentido anti-horário
		}
		turnRight(e.getBearing());
		fireBullet(2);
	//	ahead(e.getDistance() + 5);
	
		 
		if (e.getDistance() < 50 && getEnergy() > 50) {  //Robor perto com vida atira Dano 3
		fireBullet(2);
		} 
		else {
		fireBullet(2); //Robo Distante Dano 2.
		}
		//	scan(); //verificas se tem algum Robo
		}
		
	public void onHitRobot(HitRobotEvent e) 
	{
		if (e.getBearing() >= 0) 
		{  // verifica onde esta o oponente
		turnDirection = 1;          
		} 
		else 
		{
		turnDirection = -1;
		}
		turnRight(e.getBearing());

		// Atira com base na vida do outro Robo

		
		
		
		
	}
}
